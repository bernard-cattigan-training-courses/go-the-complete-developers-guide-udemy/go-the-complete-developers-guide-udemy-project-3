# Go - The Complete Developers Guide - Udemy - Project 3

Go: The Complete Developer's Guide (Golang)

Master the fundamentals and advanced features of the Go Programming Language (Golang)

https://www.udemy.com/course/go-the-complete-developers-guide/

## Project

Structs Example

## Useful

Value vs Reference Types

![img1](readMeImages/valueAndReferenceTypes.png)
